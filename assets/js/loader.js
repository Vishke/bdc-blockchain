const loader = document.querySelector('.loader');
const anchor = document.querySelectorAll('.link');

const removeLoader = () => {
  setTimeout(() => {
    document.body.removeChild(loader);
  }, 2000);
}
removeLoader();

anchor.forEach(link => {
  link.addEventListener('click', () => {
    window.location.href = "./blockchain.html"
  });
});