let boxContainer = document.querySelector('.box-container');
let boxes = document.querySelectorAll('.scrolling');
let element = document.documentElement;
console.log(element);

let clones = [];
let disableScroll = false;
let scrollHeight = 0;
let scrollPos = 0;
let clonesHeight = 0;

const getScrollPos = () => {
  return  boxContainer.scrollTop;
}

const setScrollPos = (pos) => {
  boxContainer.scrollTop = pos;
}

const getClonesHeight = () => {
  clonesHeight = 0;
  clones.forEach(clone => {
    clonesHeight += clone.offsetHeight;
  })
  return clonesHeight;
}

const reCalc = () => {
  scrollPos = getScrollPos();
  scrollHeight = boxContainer.scrollHeight;
  clonesHeight = getClonesHeight();

  if (scrollPos <= 0) {
    setScrollPos(1);
  }
}

const scrollUpdate = () => {
  if (!disableScroll) {
    scrollPos = getScrollPos();
    if (clonesHeight + scrollPos >= scrollHeight) { 
      setScrollPos(1);
      disableScroll = true;
    } else if (scrollPos <= 0) {
      setScrollPos(scrollHeight - clonesHeight);
      disableScroll = true;
    }
  }
  if (disableScroll) {
    window.setTimeout(() => {
      disableScroll = false;
    }, 40);
  }
}

const onLoad = () => {
  boxes.forEach(box => {
    const clone = box.cloneNode(true);
    boxContainer.append(clone);
    clone.classList.add('js-clone');
  });
  clones = boxContainer.querySelectorAll('.js-clone');
  reCalc();
  boxContainer.addEventListener('scroll', () => {
    window.requestAnimationFrame(scrollUpdate);
  }, false);
  window.addEventListener('resize', () => {
    window.requestAnimationFrame(reCalc);
  }, false);
}
window.onload = onLoad();


