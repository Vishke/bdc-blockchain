let links = document.querySelectorAll('.change');
let rectangle = document.querySelector('.rectangle');
let arrows = document.querySelectorAll('.arrow');
let dates = document.querySelectorAll('.text');
let dividers = document.querySelectorAll('.divider');
let youtube = document.querySelector('.youtube');
let twitter = document.querySelector('.twitter');

let main = ['#DFDFCF', '#DC346E', '#F29C9F', '#BFFDA6', '#F48D6D', '#FFF45C', '#09CACC', '#B834DC'];
let secondary = ['#444443', '#23CB91', '#0B5754', '#400259', '#0B7292', '#000882', '#F63533', '#47CB23'];
let blockchain = ["url('./assets/images/block/Block-1.svg')", "url('./assets/images/block/Block-2.svg')", "url('./assets/images/block/Block-3.svg')", "url('./assets/images/block/Block-4.svg')", "url('./assets/images/block/Block-5.svg')", "url('./assets/images/block/Block-6.svg')", "url('./assets/images/block/Block-7.svg')", "url('./assets/images/block/Block-8.svg')"];
let arrowColor = ['./assets/images/arrow/Arrow-1.svg', './assets/images/arrow/Arrow-2.svg', './assets/images/arrow/Arrow-3.svg', './assets/images/Arrow/arrow-4.svg', './assets/images/arrow/Arrow-5.svg', './assets/images/arrow/Arrow-6.svg', './assets/images/arrow/Arrow-7.svg', './assets/images/arrow/Arrow-8.svg'];
let socialColor = ['./assets/images/Group 3.svg', './assets/images/Group 3(1).svg', './assets/images/Group 3(2).svg', './assets/images/Group 3(3).svg', './assets/images/Group 3(4).svg', './assets/images/Group 3(5).svg', './assets/images/Group 3(6).svg', './assets/images/Group 3(7).svg'];
let socialColor1 = ['./assets/images/Group 2.svg', './assets/images/Group 2(1).svg', './assets/images/Group 2(2).svg', './assets/images/Group 2(3).svg', './assets/images/Group 2(4).svg', './assets/images/Group 2(5).svg', './assets/images/Group 2(6).svg', './assets/images/Group 2(7).svg'];


window.onload = () => {
    let random = Math.floor(Math.random() * main.length);
    let mainRnd = main[random];
    document.body.style.background = mainRnd;
    let secondaryRnd = secondary[random];
    links.forEach(el => {
        el.style.color = secondaryRnd;
    });
    rectangle.style.borderColor = secondary[random];
    rectangle.style.backgroundImage = blockchain[random];
    arrows.forEach(ar => {
        ar.src = arrowColor[random];
    });
    dates.forEach(date => {
        date.style.color = secondary[random];
    });
    dividers.forEach(divider => {
        divider.style.background = secondary[random];
        divider.style.borderColor = secondary[random];
    });
    document.body.style.borderColor = secondary[random];
    youtube.src = socialColor[random];
    twitter.src = socialColor1[random];
};
